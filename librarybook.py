from flask import Flask, render_template, request, session,redirect,flash,get_flashed_messages,message_flashed
app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\]/'
user = "r"

class Library:
    def __init__(self,list1):
        self.bookslist = list1
        self.name = 'GIT'
        self.lendDict={}

    def displayBooks(self):
        params=[]
        i=1
        print(f"Books in library : {self.name}\n")
        for book in self.bookslist:
            print(f"{i}:{book}")
            params.append(book)
            i +=1
        return params

    def lendBook(self,user,book):
        if book not in self.lendDict.keys():
            self.lendDict.update({book:user})
            return 1
        else:
            return self.lendDict[book]

    def addBook(self,book):
        self.bookslist.append(book)
        print("Book has been added to the list")

    def returnBook(self,book):
        self.lendDict.pop(book)

    def booktaken(self):
        print(self.lendDict)
lk = Library(['Maths','Java','Python','Physics'])

        
par = "Hello"

@app.route('/')
def hello():
    return render_template('index.html')

@app.route('/check',methods=['GET', 'POST'])
def check():
    j=0
    l1=[]
    l2=[]
    if request.method == "POST":
        user_choice = request.form['num']
        user_choice = int(user_choice)
        if user_choice == 1:
            params=lk.displayBooks()
            return render_template('displayBooks.html',params=params)
        elif user_choice == 2:
            return render_template('lendBook.html')
        elif user_choice ==3:
            return render_template('addBook.html')            
        elif user_choice ==4:
            return render_template('returnBook.html')
        elif user_choice ==5:
            jack=lk.lendDict
            for i in jack:
                j +=1
                l1.append(i)
                l2.append(jack[i])
            return render_template('booktaken.html',params1=l1,params2=l2,j=j)
    else:
        return render_template('displayBooks.html')

@app.route('/check1',methods=['GET', 'POST'])
def check1():
    if request.method == "POST":
        book = request.form['book']
        lk.addBook(book)
        return redirect('/')

@app.route('/check2',methods=['GET', 'POST'])
def check2():
    if request.method == "POST":
        bookname = request.form['bookname']
        youname = request.form['youname']
        print(youname)
        if bookname in lk.bookslist:
            name = lk.lendBook(youname,bookname)
            print(name)
            if name == 1:
                return redirect('/')
            else:
                flash(f"Book is lend to {name}\nSo check for orther books or\n come after some while")
                return render_template('lendBook.html') 
    else:
        return render_template('lendBook.html')

@app.route('/check3',methods=['GET', 'POST'])
def check3():
    if request.method == "POST":
        book = request.form['returnBook']
        lk.returnBook(book)
        return redirect('/')

@app.route('/displayBooks')
def displayBooks():
    return render_template('displayBooks.html')


@app.route('/booktaken')
def booktaken():
    return render_template('booktaken.html')

@app.route('/lendBook')
def lendBook():
    return render_template('lendBook.html')

@app.route('/addBook')
def addBook():
    return render_template('addBook.html')

@app.route('/returnBook')
def returnBook():
    return render_template('returnBook.html')


app.run(debug=True)
